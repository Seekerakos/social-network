var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var userSchema = require('../schemas/userSchema');
var postSchema = require('../schemas/postsSchema');
var User = mongoose.model('User', userSchema);
var bodyParser = require('body-parser');
// create application/json parser
var jsonParser = bodyParser.json();
var Post = mongoose.model('Post', postSchema);

router.post('/newPost', jsonParser, function (req, res) {
    var post = new Post(req.body);
    post.save(function (err, post) {
        if(!err){
            res.send(post);
        }else{
            res.send(err);
        }
    });
});

router.get('/posts/:id', function(req, res){
    Post.find({_myID : req.params.id},function(err,posts){
        console.log(req.params.id);
        if(!err){
            res.send(posts);
        }else{
            res.send(false);
        }
    })
});

module.exports = router;