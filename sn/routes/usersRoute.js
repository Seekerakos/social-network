var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var userSchema = require('../schemas/userSchema');
var authenticationSchema = require('../schemas/authenticationSchema');
var User = mongoose.model('User', userSchema);
var Auth = mongoose.model('Auth', authenticationSchema)
var sha1 = require('sha1');
var bodyParser = require('body-parser');
// create application/json parser
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({extended: false});

//Get user profile.
router.get('/profile/:username', jsonParser, function (req, res) {

    User.findOne({username: req.params.username}, function (err, user) {
        console.log(req.params.username)
        if (!err) {
            if (user) {

                res.send(user);

            } else {
                res.send('no user found');
            }
        } else {
            res.send(err);
        }
    });
});

//Login to page.
router.post('/login', jsonParser, function (req, res) {
    User.findOne({username: req.body.username}, function (err, user) {
        if (!err) {
            if (user) {

                if (user.password == sha1(req.body.password)) {
                    var authedUser = new Auth({
                            _userID: user._id,
                            timestamp: req.body.time
                        }
                    );
                    authedUser.save(function (err, auth) {
                        if (!err) {
                            console.log(auth._userID + ' ' + user._id);
                        }
                    });
                    res.send(user);
                } else {
                    res.send(false);
                }
            } else {
                res.send(false);
            }
        } else {
            res.send(err);
        }
    });
});

//Logout from page.
router.get('/logout/:id', function (req, res) {
    Auth.findOne({_userID: req.params.id}, function (err, auth) {
        if (!err) {
            if (auth) {
                console.log(auth);
                auth.remove();
                res.send('logged out');
            } else(
                res.send('already logged out')
            )

        }
    });
});

//Create new account.
router.post('/register', jsonParser, function (req, res) {
    User.findOne({username: req.body.username}, function (err, user) {
        if (!err) {
            if (!user) {
                var user = new User({
                    name: {first: req.body.first, last: req.body.last},
                    email: req.body.email,
                    username: req.body.username,
                    password: sha1(req.body.password),
                    profile_pic: 'files/default_profile_pic.jpg'
                });
                user.save(function (err) {
                    if (err) {
                        console.log('Error in Saving user: ' + err);
                        res.send(err);
                        throw err;
                    } else {
                        console.log(user.username + ' Registration successful');
                        res.send(user);

                    }
                });
            } else {
                res.send(false);
            }
        } else {
            console.log(err);
            res.send(err);
        }
    });
});

//Edit profile details.
router.put('/update', jsonParser, function (req, res) {
    User.findOne({username: req.body.usernameSearch}, function (err, user) {
        if (!err) {
            console.log(user);
            user.name = {first: req.body.first, last: req.body.last};
            user.dob = req.body.dob;
            user.job = req.body.job;
            user.save(function (err, user) {
                if (!err) {
                    res.send(user);
                } else {
                    res.send('Something went wrong');
                }
            })
        } else {
            res.send('Something went wrong');
        }
    });
});

//Change User Password.
router.put('/changePassword', jsonParser, function (req, res) {

});

router.put('/changeUsername', jsonParser, function (req, res) {

});

router.post('/hobbies', jsonParser, function (req, res) {

});

router.post('/authentication', jsonParser, function (req, res) {
    Auth.findOne({_userID: req.body.id}, function (err, auth) {
        if (!err) {
            if (auth) {
                if (auth.timestamp == req.body.time) {
                    console.log('authenticated');
                    res.send(true);
                }
            } else {
                res.send(false);
            }
        }
    });
});

router.get('/user/:id',function(req, res){
    console.log(req.params.id)
   User.findOne({_id: req.params.id}, function(err, user){
       if(!err){
           res.send(user);
           console.log(" user  " + user);
       }
   })
});
module.exports = router;