var mongoose = require('mongoose');

var Schema = mongoose.Schema
    , ObjectId = Schema.ObjectId;

var auth = new Schema({
    _userID : ObjectId,
    timestamp : Number
});

module.exports = auth;