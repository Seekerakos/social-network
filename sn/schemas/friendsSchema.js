var mongoose = require('mongoose');

var Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;

var friends = new Schema({
	_myID : ObjectId,
	_friendID : ObjectId
});

module.exports = friends;