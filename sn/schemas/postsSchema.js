var mongoose = require('mongoose');

var Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;

var posts = new Schema({
	_myID : ObjectId,
	timestamp : Number,
	text : String
});

module.exports = posts;