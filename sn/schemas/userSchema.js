var mongoose = require('mongoose');

var Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;

var user = new Schema({
	name : { first : String, last : String },
	dob : String,
	email : String,
	username : String,
	password : String,
	profile_pic : String,
	hobbies : {
		sports : String,
		music : String,
		movies : String,
		books : String
	},
	job : String
});

module.exports = user;