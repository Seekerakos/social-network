var express = require('express');
var app = express();
var mongoose = require('mongoose');
var users_routes = require('./routes/usersRoute');
var client = require('./routes/mainRoute');
var favicon = require('serve-favicon');
var post_routes = require('./routes/postsRoute');
//Connect to database Users.
mongoose.connect('mongodb://localhost/socialNetwork');


app.use(express.static(__dirname + '/www'));
////User Users API Route.
app.use('/api', users_routes);
app.use('/api', post_routes);
app.use('/', client);
app.use(favicon(__dirname + '/www/favicon.ico'));

//Initiate Server at port 80.
var server = app.listen(80, function () {
    console.log('Server started at port 80');
});
