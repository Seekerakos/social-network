var app = angular.module('app', [
    'ui.router',
    'ipCookie',
    'loginController',
    'homeController',
    'mainController',
    'profileController',
    'messagesController'
]);
app.run(['$rootScope', '$http', 'ipCookie', '$state', function ($rootScope, $http, ipCookie, $state) {
    var cookie = ipCookie('classmates');

    $http.post('/api/authentication', cookie).success(function (data) {
        if (data == false) {
            $state.go('login');
        } else {
            $http.get('/api/user/' + cookie.id).success(function (data) {
                $rootScope.user = data;
                $rootScope.logoutButton = true;
            })
        }
    }).error(function(data){
        console.log('error');
    });

    //$rootScope.logoutButton = false;
    //$state.go('login');


}]);
app.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /
    $urlRouterProvider.otherwise("/");
    //
    // Now set up the states
    $stateProvider
        .state('/', {
            url: "/",
            templateUrl: "partials/home.html",
            controller: 'homeCtrl'
        })
        .state('login', {
            url: "/login",
            templateUrl: "partials/login.html",
            controller: 'loginCtrl'
        })
        .state('profile', {
            url: '/profile/:username',
            templateUrl: 'partials/profile.html',
            controller: 'profileCtrl'
        })
        .state('messages', {
            url: '/messages',
            templateUrl: 'partials/messages.html',
            controller: 'messagesCtrl'
        });
});