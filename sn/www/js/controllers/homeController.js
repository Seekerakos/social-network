var app = angular.module('homeController', []);

app.controller('homeCtrl', ['$scope', '$http', '$rootScope', 'ipCookie', '$state',
    function ($scope, $http, $rootScope, ipCookie, $state) {
        var cookie = ipCookie('classmates');
        if (cookie) {
            $http.post('/api/authentication', cookie).success(function (data) {
                if (!data) {
                    $rootScope.user = {};
                    $state.go('login');
                }
            });
        } else {
            $state.go('login');
        }
        $scope.share = function () {
            var postData = {
                _myID : $rootScope.user._id,
                timestamp : Date.now(),
                text : $scope.postText
            }
            $http.post('api/newPost', postData).success(function(data){
               if(data != false){
                   $scope.postText = '';
               }
            });
        }
    }]);