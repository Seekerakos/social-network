var app = angular.module('loginController', []);

app.controller('loginCtrl', ['$scope', '$http', '$rootScope', '$state', 'ipCookie','$location',
    function ($scope, $http, $rootScope, $state, ipCookie, $location) {
        var cookie = ipCookie('classmates');
        $http.post('/api/authentication', cookie).success(function (data) {
            if (data) {
                $state.go('/');
            }
        });
        $scope.login = function () {
            var t = Date.now()
            $http.post('/api/login', {
                username: $scope.loginName,
                password: $scope.loginPassword,
                time: t
            }).success(function (data) {
                if (data != false) {
                    $rootScope.user = data;
                    //console.log($rootScope.user);
                    ipCookie('classmates', {id: data._id, time: t});
                    $rootScope.logoutButton = true;
                    //$location.path('/')
                    $state.go('/');
                }
            });
        };

        $scope.register = function () {
            $scope.errors = [];
            $scope.message = [];
            var reg = $scope.reg;
            if (reg.username != undefined && reg.password != undefined && reg.mail != undefined && reg.first != undefined && reg.last != undefined) {
                $http.post('/api/register', $scope.reg).success(function (data) {
                    if(data != false){
                        $scope.message = ['Registered successful login now!']
                        jQuery('#signupbox').hide();
                        jQuery('#loginbox').show();
                    }else{
                        $scope.errors = ['Username already exists!']
                    }
                });
            }else{
                $scope.errors = ['Fill all the fields']
            }
        }


    }]);