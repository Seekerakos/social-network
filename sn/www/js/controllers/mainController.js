var app = angular.module('mainController', []);

app.controller('mainCtrl', ['$scope', '$http', '$rootScope', '$state','ipCookie','$location',
    function ($scope, $http, $rootScope, $state, ipCookie) {
        //$scope.logoutButton = true;
        $scope.logout = function () {
            var cookie = ipCookie('classmates');
            $http.get('/api/logout/' + cookie.id).success(function(data){
                ipCookie.remove('classmates')
                $rootScope.user = {};
                $state.go('login');
                $scope.logoutButton = false;
            });
        };
        $scope.submit = function () {
            if ($scope.searchName != undefined && $scope.searchName != '') {
                $state.go("profile",{username:$scope.searchName});
            }
        }
    }]);