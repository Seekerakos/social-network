var app = angular.module('profileController', []);

app.controller('profileCtrl', ['$scope', '$http', '$rootScope', '$stateParams', '$location','ipCookie',
    function ($scope, $http, $rootScope, $stateParams, $location, ipCookie) {
        $http.get('api/profile/' + $stateParams.username).success(function (data) {
            if (data != 'no user found') {
                $scope.name = data.name.first + ' ' + data.name.last;
            } else {
                $location.path('/');
            }
        });
        var cookie = ipCookie('classmates');
        $http.get('api/posts/' + cookie.id).success(function(data){
            $scope.posts = data;
        });

    }]);